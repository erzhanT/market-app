const path = require("path");
const express = require('express');
const config = require("../config");
const Product = require('../models/Product');
const auth = require('../middleware/auth');
const multer = require("multer");
const {nanoid} = require("nanoid");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


const router = express.Router();

router.get('/', async (req, res) => {

    // const products = await Product.find({category: req.query.category});
    try {
        if (req.query.category) {
            const products = await Product.find({category: req.query.category});
            res.send(products);
        } else  {
            const products = await Product.find()
            res.send(products);
        }
    } catch (e) {
        res.sendStatus(500);
    }

});

router.get('/:id', async (req, res) => {

    const product = await Product.findById(req.params.id).populate({path: "user"});
    if (product) {
        res.send(product);
    } else {
        res.sendStatus(404);
    }
});

router.post('/', auth, upload.single("image"), async (req, res) => {

    try {
        console.log(req.user);
        const productData = req.body;
        if (req.file) {
            productData.image = req.file.filename;
        }
        productData.user = req.user._id;
        const product = new Product(productData)
        await product.save();
        res.send(product);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {

    const product = await Product.findById({_id: req.params.id}).populate({path: "user"});
    if (req.user._id.toString() === product.user._id.toString()) {
        const result = await Product.findByIdAndDelete({_id: req.params.id});
        if (result) {
            res.send("Sold!");
        }
    } else {
        res.status(403).send({error: "Unauthorized"});
    }
});

module.exports = router;