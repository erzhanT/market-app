const mongoose = require("mongoose");



const ProductSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true,
        min: [1, 'Minimum quantity is one']
    },
    description: {
        type: String,
        required: true
    },
    image: String,
    category: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
});

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;