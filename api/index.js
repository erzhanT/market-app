const express = require("express");
const mongoose = require("mongoose");
const users = require("./app/users");
const products = require('./app/products');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const config = require('./config');


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

app.use("/products", products);
app.use("/users", users);

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
};

run().catch(console.error);