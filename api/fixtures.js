const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");
const User = require("./models/User");
const Product = require("./models/Product");




const run =  async () => {

    await mongoose.connect(config.db.url, config.db.options);
    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user, user1] = await User.create({
        username: "user",
        password: "1234",
        displayName: "User",
        phone: 7000010798,
        token: nanoid()
    }, {
        username: "user1",
        password: "1234",
        displayName: "User1",
        phone: 7000010798,
        token: nanoid()
    });

    await Product.create({
            title: "McLaren",
            category: "cars",
            user: user._id,
            price: 225,
            description: "product description",
            image: "car.webp"
        }, {
            title: "Desert Eagle",
            category: "guns",
            user: user._id,
            price: 567,
            description: "product description",
            image: "gun.jpeg"
        },{
            title: "Jeans",
            category: "clothes",
            user: user1._id,
            price: 123,
            description: "product description",
            image: "clothes.jpeg"
        },{
            title: "TLC 200",
            category: "cars",
            user: user1._id,
            price: 225,
            description: "product description",
            image: "car.webp"
        },{
            title: "M416",
            category: "guns",
            user: user._id,
            price: 8900,
            description: "product description",
            image: "gun.jpeg"
        },{
            title: "Ferrari 412",
            category: "cars",
            user: user._id,
            price: 345,
            description: "product description",
        },{
            title: "MacBook Pro",
            category: "computers",
            user: user1._id,
            price: 225,
            description: "product description",
            image: "PC.jpeg"
        },
    );
    await mongoose.connection.close();

};

run().catch(console.error);