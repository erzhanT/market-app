import React from 'react';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Products from "./containers/Products/Products";
import CurrentProduct from "./containers/CurrentProduct/CurrentProduct";
import AddProduct from "./containers/AddProduct/AddProduct";

function App() {
  return (
      <div className="App">
        <Layout/>
        <Switch>
          <Route path="/" exact component={Products}/>
          <Route path="/products/:id" exact component={CurrentProduct}/>
          <Route path="/addproduct" component={AddProduct}/>
          <Route path="/signup" exact component={Register}/>
          <Route path="/signin" exact component={Login}/>
          <Route render={() => <h1>404</h1>}/>
        </Switch>
      </div>
  );
}

export default App;
