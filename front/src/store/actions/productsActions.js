import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";

export const FETCH_PRODUCT_SUCCESS = "FETCH_PRODUCT_SUCCESS";
export const FETCH_PRODUCT_ERROR = "FETCH_PRODUCT_ERROR";

export const ADD_PRODUCT_SUCCESS = "ADD_PRODUCT_SUCCESS";
export const ADD_PRODUCT_ERROR = "ADD_PRODUCT_ERROR";


const fetchProductsSuccess = value => ({type: FETCH_PRODUCTS_SUCCESS, value});

const fetchProductsError = error => ({type: FETCH_PRODUCTS_ERROR, error});

const addProductSuccess = value => ({type: ADD_PRODUCT_SUCCESS, value});
const addProductError = error => ({type: ADD_PRODUCT_ERROR, error})
const fetchProductSuccess = value => ({type: FETCH_PRODUCT_SUCCESS, value});
const fetchProductError = error => ({type: FETCH_PRODUCT_ERROR, error});




export const fetchProductsByCategory = (cat) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("products?category=" + cat);
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductsError(e));
        }
    };
};



export const addProduct = (data) => {
    return async (dispatch, getState) => {
        console.log(getState().users);
        const headers = {
            "Authorization": getState().users.user && getState().users.user.token
        };
        console.log(headers);
        try {
            await axiosApi.post('/products', data, {headers});
            dispatch(addProductSuccess());
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(addProductError(e));
        }
    };
};



export const fetchProduct = (id) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/products/" + id);
            dispatch(fetchProductSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductError(e));
        }
    };
};


export const deleteProduct = (id) => {
    return async (dispatch, getState) => {
        const headers = {
            "Authorization": getState().users.user && getState().users.user.token
        };
        try {
            await axiosApi.delete("/products/" + id, {headers});
            dispatch(historyPush("/"));
        } catch (e) {
            console.log(e);
        }
    }
}