import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGISTER_USER_FAILURE = "REGISTER_USER_FAILURE";

export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAILURE = "LOGIN_USER_FAILURE";

export const LOGOUT_USER = "LOGOUT_USER";


const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});


export const registerUser = userData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post("/users", userData);
            dispatch(registerUserSuccess(response.data));
            console.log(response.data);
            dispatch(historyPush("/"));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(registerUserFailure(e.response.data));
            } else {
                dispatch(registerUserFailure(e));
            }
        }
    }
};


export const loginUser = userData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post("/users/sessions", userData);
            dispatch(loginUserSuccess(response.data));
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(loginUserFailure(e.response.data));
        }
    }
};

export const logoutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {"Authorization": token};

        await axiosApi.delete("/users/sessions", {headers});
        dispatch({type: LOGOUT_USER});
        dispatch(historyPush("/"));
    };
};