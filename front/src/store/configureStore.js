import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import usersReducer from "./reducers/usersReducer";
import productsReducer from "./reducers/productsReducer";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const rootReducer = combineReducers({
    products: productsReducer,
    users: usersReducer,
});



const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunkMiddleware)));


store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users
    });
});

export default store;
